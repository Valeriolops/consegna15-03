package com.progettoChatClassi;

import java.util.Date;

public class Chat {
	
	
	
	
	private Integer messagioId;
	private String testo;
	private Date orario;
	private String nickname_chat;
	
	public Chat()
	{
		
	}

	public Integer getMessagioId() {
		return messagioId;
	}

	public void setMessagioId(Integer messagioId) {
		this.messagioId = messagioId;
	}

	public String getTesto() {
		return testo;
	}

	public void setTesto(String testo) {
		this.testo = testo;
	}

	public Date getOrario() {
		return orario;
	}

	public void setOrario(Date orario) {
		this.orario = orario;
	}

	public String getNickname_chat() {
		return nickname_chat;
	}

	public void setNickname_chat(String nickname_chat) {
		this.nickname_chat = nickname_chat;
	}

	@Override
	public String toString() {
		return "Chat [messagioId=" + messagioId + ", testo=" + testo + ", orario=" + orario + ", nickname_chat="
				+ nickname_chat + "]";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

}

package progettoChat.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.progettoChat.connessione.ConnettoreDB;
import com.progettoChatClassi.Chat;

public class ChatDAO implements Dao<Chat> {

	@Override
	public Chat getById(int id) throws SQLException {
		return null;
	}

	@Override
	public ArrayList<Chat> getAll() throws SQLException {

		ArrayList<Chat> elencoMessaggi = new ArrayList<Chat>();

		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = " select  chat.messagioId, chat.testo, chat.orario, chat.nickname_chat  from persona,chat "
				+ "      where persona.nickname = chat.nickname_chat   " + "      order by chat.orario asc   ";//DESC
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

		ResultSet risultato = ps.executeQuery();

		while (risultato.next()) {

			Chat cTemp = new Chat();
			cTemp.setMessagioId(risultato.getInt(1));
			cTemp.setTesto(risultato.getString(2));
			cTemp.setOrario(risultato.getDate(3));
			cTemp.setNickname_chat(risultato.getString(4));
			elencoMessaggi.add(cTemp);

		}

		return elencoMessaggi;
	}

	@Override
	public void insert(Chat t) throws SQLException {

		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = " insert into chat (testo, orario, nickname_chat)  values (?,?,? )";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getTesto());
		java.util.Date utilStartDate = t.getOrario();
		java.sql.Date sqlStartDate = new java.sql.Date(utilStartDate.getTime());
		ps.setDate(2, sqlStartDate);
		ps.setString(3, t.getNickname_chat());
		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();
		t.setMessagioId(risultato.getInt(1));

	}

	@Override
	public boolean delete(Chat t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Chat t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
